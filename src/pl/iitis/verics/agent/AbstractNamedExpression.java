/*
 * AbstractNamedExpression.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A named expression.
 * 
 * @author Artur Rataj
 */
public class AbstractNamedExpression implements SourcePosition {
    /**
     * Position of this variable in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * Name of this definition.
     */
    public final String NAME;
    /**
     * Expression in this definition.
     */
    public AbstractExpression EXPR;
    
    /**
     * Creates a new definition.
     * 
     * @param pos position in the source stream
     * @param name name
     * @param expr expression
     */
    public AbstractNamedExpression(StreamPos pos, String name, AbstractExpression expr) {
        setStreamPos(pos);
        NAME = name;
        EXPR = expr;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
}
