/*
 * ValueOrFormula.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.iitis.verics.agent.*;

/**
 * Constants either <code>AsTyped</code> or <code>AsFormula</code>.
 * A helper wrapper for the Vxsp parser.
 * 
 * @author Artur Rataj
 */

public class ValueOrFormula {
//    /**
//     * Identifier of this value or formula. Used in the semantic check to find
//     * the referenced variable, then becomes null.
//     */
//    final public String ID;
    /**
     * Not null if this wrapper contains a value. Before a semantic check,
     * <code>AsVariable</code> can mean either a variable or an action
     * expression.
     */
    final public AsTyped VALUE;
    /**
     * Not null if this wrapper contains a formula.
     */
    final public AsFormula FORMULA;
    
//    /**
//     * Creates a wrapper for an inentifier, to be replaced with a variable.
//     * during a semantic check.
//     * 
//     * @param id identifier; must be qualified as unqualified ones are resolved
//     * during parsing
//     */
//    public ValueOrFormula(String id) {
//        if(!id.contains("."))
//            throw new RuntimeException("qualified identified expected");
//        ID = id;
//        VALUE = null;
//        FORMULA = null;
//    }
    /**
     * Creates a wrapper for a value.
     * 
     * @param value the value to wrap
     */
    public ValueOrFormula(AsTyped value) {
        VALUE = value;
        FORMULA = null;
    }
    /**
     * Creates a wrapper for a formula.
     * 
     * @param formula the formula to wrap
     */
    public ValueOrFormula(AsFormula formula) {
        VALUE = null;
        FORMULA = formula;
    }
}
