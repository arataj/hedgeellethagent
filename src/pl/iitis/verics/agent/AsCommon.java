/*
 * AsCommon.java
 *
 * Created on Mar 21, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * If a symbol is private, global or accessible to selected modules.
 * 
 * @author Artur Rataj
 */
public class AsCommon {
    /**
     * Position of this definition within the source code. Null if not
     * constructed from source code.
     */
    public final StreamPos pos;
    /**
     * Names of modules, null for all. Converted into <code>KNOWN</code>
     * within <code>complete()</code>. Null if not constructed from source
     * code.
     */
    public List<String> knownProto;
    /**
     * List of modules, to which this symbol is known. A local symbol
     * typically has its owner module listed, otherwise not known locally.
     */
    public List<AsModule> KNOWN;
    /**
     * If this symbol can be accessed globally, i.e. <code>KNOWN</code>
     * contains all modules in the agent system.
     */
    public boolean GLOBAL;
    /**
     * If this symbol is private, i.e. <code>KNOWN</code> is empty or
     * contains only  the owner module.
     */
    public boolean PRIVATE;
    
    /**
     * <p>Creates a new symbol access definition. A complete object needs
     * <code>complete()</code> after the whole agent system has been
     * constructed.</p>
     * 
     * <p>To be used by the parser.</p>
     * 
     * @param pos position of this definition within the source code; null if not
     * constructed from the source code
     * @param known eventually transformed name--to--module into
     * <code>KNOWN</code> unless null; if null, global access is assumed
     * and thus as modules are added to <code>KNOWN</code> within
     * <code>complete()</code>
     */
    public AsCommon(StreamPos pos, List<String> known) {
        this.pos = pos;
        knownProto = known;
    }
    /**
     * <p>Creates a new symbol access definition, limited to the local
     * module.</p>
     * 
     * <p>To be used by the parser.</p>
     * 
     * @param pos position of this definition within the source code; null if not
     * constructed from the source code
     */
    public AsCommon(StreamPos pos) {
        this.pos = pos;
        knownProto = null;
        PRIVATE = true;
    }
    /**
     * Creates a new symbol access definition. Typicaly not used by the
     * grammatical parsers, which only known module names at the stage
     * of constructing this object.
     * 
     * @param known deeply copied to </code>KNOWN</code>
     */
    public AsCommon(List<AsModule> known) {
        pos = null;
        knownProto = null;
        KNOWN = new LinkedList<>(known);
    }
    /**
     * After the agent system has been constructed by parsing, this
     * method must be called to complete this access definition.
     * 
     * @param as an agent system within the call to completeParse()
     * @param owner owner of the symbol, whose access rights are
     * represented by this object
     */
    public void completeParse(AgentSystem as, AsModule owner) throws ParseException {
        if(PRIVATE) {
            KNOWN = new LinkedList<>();
            KNOWN.add(owner);
        } else if(knownProto == null)
            KNOWN = new LinkedList<>(as.moduleList);
        else {
            KNOWN = new LinkedList<>();
            for(String name : knownProto) {
                AsModule m = as.lookupModule(name);
                if(m == null)
                    throw new ParseException(pos, ParseException.Code.LOOK_UP,
                            "module not found: " + name);
                if(KNOWN.contains(m))
                    throw new ParseException(pos, ParseException.Code.DUPLICATE,
                            "duplicate module name: " + name);
                KNOWN.add(m);
            }
            GLOBAL = KNOWN.size() == as.moduleList.size();
            PRIVATE = KNOWN.isEmpty() ||
                    (KNOWN.size() == 1 && KNOWN.get(0) == owner);
        }
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("[");
        if(KNOWN != null) {
            for(AsModule m : KNOWN)
                out.append(" " + m.NAME);
        } else {
            out.append(" proto");
            for(String s : knownProto)
                out.append(" " + s);
        }
        out.append(" ]");
        if(GLOBAL)
            out.append(" GLOBAL");
        if(PRIVATE)
            out.append(" PRIVATE");
        return out.toString();
    }
}
