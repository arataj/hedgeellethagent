/*
 * AsModule.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A module, e.g. an environment or an agent.
 * 
 * @author Artur Rataj
 */
public class AsModule implements SourcePosition {
    /**
     * Position of this moule in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * If this module is an agent, and thus its variables can not be
     * public.
     */
    public final boolean FORCE_PRIVATE_VARIABLES;
    /**
     * Name of this module.
     */
    public final String NAME;
    /**
     * Variables. All must be global for an environment, none can be global for an agent.
     */
    public List<AsVariable> variableList;
    /**
     * Variables, keyed with names.
     */
    protected SortedMap<String, AsVariable> variables;
    /**
     * Actions, both in the protocol and in the statements.
     */
    public final SortedSet<String> ACTIONS;
    /**
     * Protocol.
     */
    public final AsProtocol PROTOCOL;
    /**
     * Subsequent statements in this module.
     */
    public final List<AsStatement> STATEMENTS;
    /**
     * Synchronizing labels.
     */
    public final Set<AsLabel> LABELS;
    /**
     * All variables read by expressions in this module. Null if not yet
     * computed.
     */
    public SortedSet<AsVariable> usedVars;

    /**
     * Creates a new module.
     * 
     * @param pos position in the parsed stream
     * @param forcePrivateVariables no variable of this module can be made
     * public
     * @param name name of this modul
     */
    public AsModule(StreamPos pos, boolean forcePrivateVariables, String name) {
        setStreamPos(pos);
        FORCE_PRIVATE_VARIABLES = forcePrivateVariables;
        NAME = name;
        variableList = new ArrayList<>();
        variables = new TreeMap<>();
        ACTIONS = new TreeSet<>();
        PROTOCOL = new AsProtocol(pos, this);
        STATEMENTS = new ArrayList<>();
        LABELS = new TreeSet<>();
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Adds a variable to this module.
     * 
     * @param variable variable
     */
    public void addVariable(AsVariable variable) {
        variableList.add(variable);
        variables.put(variable.NAME, variable);
    }
    /**
     * Returns a variable by its name.
     * @param name name of the variable to look up
     * @return a variable or null if not found
     */
    public AsVariable lookupVariable(String name) {
        return variables.get(name);
    }
    @Override
    public String toString() {
        return NAME;
    }
}
