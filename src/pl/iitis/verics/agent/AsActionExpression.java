/*
 * AsActionExpression.java
 *
 * Created on Mar 25, 2015
 *
 * Copyright (c) 2016 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Visitor;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.iitis.verics.agent.*;

/**
 * An action expression.
 *
 * @author Artur Rataj
 */
public class AsActionExpression extends AbstractExpression {
    /**
     * An agent system, to which this expression belongs.
     */
    public final AgentSystem AS;
    public AsModule MODULE;
    public String ACTION;
    
    /**
     * Creates a new action expression.
     * 
     * @param as agent system
     * @param pos position in the source code
     * @param outerScope scope
     * @param module module required to perform the action; if null,
     * <code>action</code> should be fully qualified and a semantic
     * check is needed
     * @param action the action's name
     */
    public AsActionExpression(AgentSystem as,
            StreamPos pos, BlockScope outerScope,
            AsModule module, String action) {
        super(pos, outerScope);
if(pos == null)
    pos = pos;
        AS = as;
        MODULE = module;
        ACTION = action;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    /**
     * A fully qualified name.
     * 
     * @return a name which contains the owner's module name and this action's
     * own name
     */
    public String getQualifiedName() {
        return MODULE.NAME + "." + ACTION;
    }
    @Override
    public AsActionExpression clone() {
        AsActionExpression copy = (AsActionExpression)super.clone();
        return copy;
    }
    /**
     * A local version of <code>toString</code>, strips the qualification
     * if unnecessary.
     * 
     * @param within module, within whose definition this textual representation
     * is going to be printed; null if always print the full qualification
     * @return textual representation, according to the MCMAS format
     */
    public String toString(AsModule within) {
        String qualification;
        if(within == MODULE)
            qualification = "";
        else
            qualification = MODULE.NAME + ".";
        return qualification + "Action = " + ACTION;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
