/*
 * AsCoalition.java
 *
 * Created on Oct 18, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

/**
 * A colaition of players, including a single player.
 * 
 * @author Artur Rataj
 */
public class AsCoalition {
    /**
     * If this coalition is solitary, i.e. created from a single agent, not being
     * a part of a single--element list.
     */
    public final boolean SOLITARY;
    /**
     * Players within this coalition.
     */
    public final List<AsModule> PLAYERS;
    
    /**
     * Creates a solitary coalition, that contains a single player.
     * 
     * @param player the module to form this solitary coalition
     */
    public AsCoalition(AsModule player) {
        SOLITARY = true;
        PLAYERS = new ArrayList<>();
        PLAYERS.add(player);
    }
    /**
     * Creates a non--solitary coalition, that contains no players at all,
     * a single player or more players.
     * 
     * @param player the modules to form this solitary coalition; the collection
     * can be empty
     */
    public AsCoalition(Collection<AsModule> players) {
        SOLITARY = false;
        PLAYERS = new ArrayList<>();
        PLAYERS.addAll(players);
    }
    /**
     * Returns a solitary player. Can be called only on a solitary coalition.
     * 
     * @return a module
     */
    public AsModule getSolitary() {
        if(SOLITARY)
            return PLAYERS.iterator().next();
        else
            throw new RuntimeException("coalition not solitary");
    }
    @Override
    public String toString() {
        if(SOLITARY)
            return PLAYERS.get(0).NAME;
        else {
            StringBuilder out = new StringBuilder();
            out.append("<");
            boolean first = true;
            for(AsModule m : PLAYERS) {
                if(!first)
                    out.append(", ");
                out.append(m.NAME);
                first = false;
            }
            out.append(">");
            return out.toString();
        }
    }
}
