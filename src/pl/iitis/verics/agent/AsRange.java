/*
 * AsRange.java
 *
 * Created on Sep 23, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

//import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;

/**
 * A range of integer arithmetic values. The pair of constants, the pair of
 * constant expressions and the set, are three mutually exclusive elements.
 * 
 * @author Artur Rataj
 */
public class AsRange implements Cloneable {
//    /**
//     * Minimum value or <code>Double.NaN</code> if an expression or a set
//     * is used instead, not evaluated yet.
//     */
//    public double min;
//    /**
//     * Maximum value or <code>Double.NaN</code> if an expression or a set
//     * is used instead, not evaluated yet.
//     */
//    public double max;
//    /**
//     * A presumably constant expression, to compute <code>min</code>.
//     */
//    public AbstractExpression minExpr;
//    /**
//     * A presumably constant expression, to compute <code>min</code>.
//     */
//    public AbstractExpression maxExpr;
    /**
     * A set of discrete values, null if <code>min != Double.NaN</code> or
     * <code>minExpr != null</code>.
     */
    public SortedSet<AsConstant> set;
    
//    /**
//     * Creates a new range, with computed extrema.
//     * 
//     * @param min minimum value
//     * @param max maximum value
//     */
//    public AsRange(double min, double max) {
//        this.min = min;
//        this.max = max;
//    }
//    /**
//     * Creates a new range, with extrema to be computed later.
//     * 
//     * @param min minimum value
//     * @param max maximum value
//     */
//    public AsRange(AbstractExpression min, AbstractExpression max) {
//        this.min = Double.NaN;
//        this.max = Double.NaN;
//        this.minExpr = min;
//        this.maxExpr = max;
//    }
    /**
     * Creates a new range, with a set of discrete values allowed.
     * 
     * @param collection a collection of values
     */
    public AsRange(Collection<AsConstant> collection) {
//        this.min = Double.NaN;
//        this.max = Double.NaN;
        set = new TreeSet<>();
        for(AsConstant c : collection)
            set.add(c);
    }
    @Override
    public AsRange clone() {
        AsRange copy;
        try {
            copy = (AsRange)super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("unexpected");
        }
//        if(minExpr != null)
//            copy.minExpr = minExpr.clone();
//        if(maxExpr != null)
//            copy.maxExpr = maxExpr.clone();
        if(set != null)
            copy.set = new TreeSet<>(set);
        return copy;
    }
    @Override
    public String toString() {
//        if(Double.isNaN(min)) {
//            if(minExpr == null) {
                String s = "[ ";
                for(AsConstant c : set)
                    s += c.toString() + " ";
                s += "]";
                return s;
//            } else
//                return "<" + minExpr.toString() + "," + maxExpr.toString() + ">";
//        } else
//            return "<" + min + "," + max + ">";
    }
}
