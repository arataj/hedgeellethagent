/*
 * AsStatement.java
 *
 * Created on Oct 14, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A single statement.
 * 
 * @author Artur Rataj
 */
public class AsStatement implements SourcePosition {
    /**
     * Position of this variable in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * Owner of this statement.
     */
    public final AsModule OWNER;
    /**
     * Label, null for none.
     */
    public final AsLabel LABEL;
    /**
     * A boolean guard expression.
     */
    public AbstractExpression GUARD;
    /**
     * Left side of the update expression, null for no update.
     */
    public AsVariable UPDATE_LEFT;
    /**
     * Right side of the update expression, null for no update.
     */
    public AbstractExpression UPDATE_RIGHT;
    
    /**
     * Creates a new statement.
     * 
     * @param pos position in the source
     * @param owner owner module
     * @param label synchronizing label, null for none
     * @param guard a conditional guard expression
     * @param updateLeft left side of the update expression, null for no update.
     * @param updateRight right side of the update expression, null for no
     * update
     */
    public AsStatement(StreamPos pos, AsModule owner, AsLabel label,
            AbstractExpression guard,
            AsVariable updateLeft, AbstractExpression updateRight) {
        setStreamPos(pos);
        OWNER = owner;
        LABEL = label;
        GUARD = guard;
        UPDATE_LEFT = updateLeft;
        UPDATE_RIGHT = updateRight;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public String toString() {
        String s = "[" + (LABEL != null ? LABEL.toString() : "") + "] " +
                GUARD.toString()  + " -> ";
        if(UPDATE_LEFT != null)
            s += UPDATE_LEFT + " := " + UPDATE_RIGHT;
        else
            s += "true";
        return s;
    }
}
