/*
 * AsOtherExpression.java
 *
 * Created on Mar 25, 2015
 *
 * Copyright (c) 2016 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Visitor;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A protocol's 'other' guard.
 *
 * @author Artur Rataj
 */
public class AsOtherExpression extends AbstractExpression {
    /**
     * A common name of all other expressions.
     */
    public static final String NAME = "Other";
    
    /**
     * Creates a new action expression.
     * 
     * @param as agent system
     * @param pos position in the source code
     * @param outerScope scope
     */
    public AsOtherExpression(StreamPos pos, BlockScope outerScope) {
        super(pos, outerScope);
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public AsOtherExpression clone() {
        AsOtherExpression copy = (AsOtherExpression)super.clone();
        return copy;
    }
    @Override
    public String toString() {
        return NAME;
    }
}
