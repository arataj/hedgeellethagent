/*
 * AsVariable.java
 *
 * Created on Sep 23, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.LocalFlags;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A variable, integer or double. Also, a holder of a variable name, to be
 * replaced during the semantic check.
 * 
 * @author Artur Rataj
 */
public class AsVariable implements Cloneable, SourcePosition, AsTyped, Comparable {
    /**
     * Position of this variable in the parsed stream; null for a yet not found
     * variable.
     */
    protected StreamPos pos;
    /**
     * Access definition.
     */
    public final AsCommon COMMON;
    /**
     * Name of this value, unqualified, null for none.
     */
    public final String NAME;
    /**
     * Owner of this variable; null for a yet not found
     * variable.
     */
    public final AsModule OWNER;
    /**
     * Possible range of values. A single value in the case of a constant.
     * Null for a yet not found variable.
     */
    public AsRange RANGE;
    /**
     * If floating; meaningless in the case of a not yet found variable.
     */
    final boolean floating;
    /**
     * If boolean; meaningless in the case of a not yet found variable.
     */
    final boolean booleaN;
    /**
     * Initial values; null for a yet not found variable.
     */
    public AsInitValue INIT;
    /**
     * A cached instance of the object returned by <code>toVariable()</code>.
     */
    private Variable CACHED_VARIABLE;
    
    /**
     * <p>Creates a new variable or a holder of a qualified variable name. Needs
     * <code>complete()</code> after the whole agent system has been constructed.</p>
     * 
     * <p>A holder should have all null except for the position and the name.</p>
     * 
     * @param pos position in the parsed stream
     * @param owner owner module; null if this object only holds a name
     * to look up within a semantic check
     * @param name name of the variable; if only a holder, then the name must be
     * qualified; otherwise it can not be qualified, as the qualification if provided
     * by <code>owner</code>
     * @param common access rights; do not need to be complete
     * @param range permitted range of values; meaningless if
     * <code>owner == null</code>
     * @param value inital value of this variable; determines type; meaningless if
     * <code>owner == null</code>
     */
    public AsVariable(StreamPos pos, AsModule owner, 
            String name, AsCommon common, AsRange range, AsInitValue value) {
        if(name == null)
            throw new RuntimeException("variable requires a name");
        setStreamPos(pos);
        OWNER = owner;
        NAME = name;
        COMMON = common;
        if(OWNER != null) {
            this.floating = value.isFloating();
            this.booleaN = value.isBoolean();
            RANGE = range;
            INIT = value;
        } else {
            floating = false;
            booleaN = false;
            RANGE = null;
            INIT = null;
        }
    }
    /**
     * Completes this variable after parsing.
     * 
     * @param as an agent system within the call to completeParse()
     */
    public void completeParse(AgentSystem as) throws ParseException {
        COMMON.completeParse(as, OWNER);
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public boolean isFloating() {
        return floating;
    }
    @Override
    public boolean isBoolean() {
        return booleaN;
    }
    @Override
    public String getTypeId() {
        return AsTyped.getTypeId(isFloating(), isBoolean());
    }
    /**
     * Returns a Verics--style type.
     * 
     * @return type of this variable
     */
    public Type getType() {
        if(floating)
            return new Type(Type.PrimitiveOrVoid.FLOAT);
        else if(booleaN)
            return new Type(Type.PrimitiveOrVoid.BOOLEAN);
        else
            return new Type(Type.PrimitiveOrVoid.INT);
    }
    /**
     * Converts this variable to a Verics--compatible equivalent of type
     * <code>Type.PrimitiveOrVoid.INT</code>. Caches the value, so that
     * only a unique instance is returned each time.
     * 
     * @return an equivalent compatible with <code>AbstractExpression</code>
     */
    public Variable toVariable() {
        if(CACHED_VARIABLE == null) {
            CACHED_VARIABLE = new Variable(getStreamPos(), null, new Type(Type.PrimitiveOrVoid.INT),
                OWNER != null ? getQualifiedName() : NAME);
            CACHED_VARIABLE.flags = new LocalFlags();
        }
        return CACHED_VARIABLE;
    }
    /**
     * Returns a qualified name of this variable.
     * 
     * @return a fully qualified name within a single system of agents
     */
    public String getQualifiedName() {
        return OWNER.NAME + "." + NAME;
    }
    @Override
    public int compareTo(Object o) {
        return getQualifiedName().compareTo(((AsVariable)o).getQualifiedName());
    }
    @Override
    public AsVariable clone() {
        AsVariable copy;
        try {
            copy = (AsVariable)super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("unexpected");
        }
        copy.pos = new StreamPos(pos);
        copy.RANGE = RANGE.clone();
        copy.INIT = INIT.clone();
        return copy;
    }
    @Override
    public String toString() {
        return "(" + getQualifiedName() + "{" + RANGE.toString() + "})";
    }
}
