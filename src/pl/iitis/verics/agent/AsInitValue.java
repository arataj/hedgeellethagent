/*
 * AsInitValue.java
 *
 * Created on Oct 1, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;
import java.util.Map.Entry;
        
/**
 * Definition of initial values.
 * 
 * @author Artur Rataj
 */
public class AsInitValue implements Cloneable {
    /**
     * If floating.
     */
    final boolean floating;
    /**
     * If boolean.
     */
    final boolean booleaN;
    /**
     * A space, 0 for the universal one.
     */
    public SortedSet<Integer> SPACE;
    /**
     * Values keyed with a space.
     */
    public Map<Integer, SortedSet<AsConstant>> VALUE;
    
    /**
     * Creates a new definition of initial values.
     */
    public AsInitValue(boolean floating, boolean booleaN) {
        SPACE = new TreeSet<>();
        VALUE = new TreeMap<>();
        this.floating = floating;
        this.booleaN = booleaN;
    }
    /**
     * Returns if this value is floating.
     * 
     * @return if boolean
     */
    public boolean isFloating() {
        return floating;
    }
    /**
     * Returns if this value is boolean.
     * 
     * @return if boolean
     */
    public boolean isBoolean() {
        return booleaN;
    }
    /**
     * Adds all contents of another <code>AsInitValue</code> to this.
     * 
     * @param init initial values
     */
    public void add(AsInitValue init) {
        for(Entry<Integer, SortedSet<AsConstant>> e : init.VALUE.entrySet())
            add(e.getKey(), e.getValue());
    }
    /**
     * Adds values to a given space.
     * 
     * @param space space, 0 for universal
     * @param values a collection of values to add
     */
    public void add(int space, Collection<AsConstant> value) {
        if(!SPACE.contains(space)) {
            SPACE.add(space);
            VALUE.put(space, new TreeSet<>());
        }
        SortedSet<AsConstant> set = VALUE.get(space);
        set.addAll(value);
    }
    /**
     * Return a filtering alternative of constants, represented
     * by this set of init values.
     * 
     * @param range set of possible values of the variable; if equal to this
     * set, then no filtering needed, and thus an empty list is returned
     * @param space space, 0 for universal
     * @return components within the alternative; empty list for true
     */
    public List<AsConstant> get(AsRange range, int space) {
        List<AsConstant> out = new LinkedList<>();
        SortedSet<AsConstant> set = VALUE.get(space);
        if(!set.containsAll(range.set)) {
            out.addAll(set);
        }
        return out;
    }
    @Override
    public AsInitValue clone() {
        AsInitValue copy;
        try {
            copy = (AsInitValue)super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("unexpected");
        }
        copy.SPACE = new TreeSet<>(SPACE);
        copy.VALUE = new TreeMap<>(VALUE);
        return copy;
    }
}
