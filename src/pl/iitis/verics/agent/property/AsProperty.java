/*
 * AsProperty.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent.property;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A property of an agent system, translated to ATL.
 * 
 * @author Artur Rataj
 */
public class AsProperty implements SourcePosition {
    public static enum Type {
        LTL,
        LTLK,
        LTLKD,
        ACTL,
        ACTLK,
        ACTLKD,
        ACTLA,
        ACTLAK,
        ACTLAKD,
        ELTL,
        ELTLK,
        ELTLKD,
        ECTL,
        ECTLK,
        ECTLKD,
        ECTLA,
        ECTLAK,
        ECTLAKD,
        CTLA,
        CTLAK,
        CTLAKD;
    }
    /**
     * Position in the parsed stream.
     */
    StreamPos pos;
    /**
     * Type of this property.
     */
    public final Type TYPE;
    /**
     * Name of this property.
     */
    public final String NAME;
    /**
     * Expression of this property.
     */
    public AbstractExpression EXPR;

    public AsProperty(StreamPos pos, Type type, String name) {
        setStreamPos(pos);
        TYPE = type;
        NAME = name;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
}
