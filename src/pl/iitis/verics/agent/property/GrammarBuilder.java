/*
 * GrammarBuilder.java
 *
 * Created on Sep 26, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent.property;

import java.io.*;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;

/**
 * Generates an Antlr4 grammar of properties supported by <code>Generator</code>.
 * 
 * @author Artur Rataj
 */
public class GrammarBuilder {
    final static String GET_POS = "{ $pos = getStreamPos(); }";
    final static String GET_POS_2 = "{ $pos2 = getStreamPos(); }";
    final static String RECURSION_CODE =
            "\t\t\t    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); ";
    final static String RECURSION_CODE_XI =
            "\t\t\t    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); ";
    
    private static String pName(String name, Generator.P p) {
        String s;
if(p == null)
    p = p;
        switch(p) {
            case PHI:
                s = "";
                break;
                
            case PSI:
                s = "_Psi";
                break;

            case PSI_SUB:
                s = "_Psi_sub";
                break;
                
            case XI:
                s = "_Xi";
                break;
                
            default:
                throw new RuntimeException("unknown argument");
        }
        return name + s + "[$as]";
    }
    private static String opAssignment(AbstractOperator op) {
        return "\n\t\t\t\t'" + op.toString() + "' { $op = " + AbstractOperator.enumId(op) + "; } ";
    }
    private static String pToken(Generator.T t) {
        String s = "";
        switch(t) {
            case FALSE:
            case TRUE:
                return "'" + t.name().toLowerCase() + "' " + GET_POS;
            case PRIMARY:
                return "expr = relationalExpression[$as, false]";
            case NEG:
                return "'!' " + GET_POS;
            case AND:
                return opAssignment(BinaryExpression.Op.CONDITIONAL_AND);
            case OR:
                return opAssignment(BinaryExpression.Op.CONDITIONAL_OR);
            case X:
            case F:
            case G:
                return "'" + t.name() + "' " + GET_POS;
            case E:
            {
                String l = "'" + t.name() + "'";
                return l + " " + GET_POS;
            }
            case A:
            {
                String l = "'" + t.name() + "'";
                return l + " " + GET_POS;
            }
            case U:
                return opAssignment(BinaryExpression.Op.U);
            case R:
                return opAssignment(BinaryExpression.Op.R);
            case IMPLICATION:
                return opAssignment(BinaryExpression.Op.IMPLICATION);
            case EQUIVALENCE:
                return opAssignment(BinaryExpression.Op.EQUIVALENCE);
            case EX:
            case EF:
            case EG:
            case AX:
            case AF:
            case AG:
            {
                char first = t.name().charAt(0);
                String compl;
//                if(first == 'A')
//                    compl = "'!' 'E'";
//                else
//                    compl = "'!' 'A'";
                char second = t.name().charAt(1);
//                return "LOOKAHEAD( '" + t.name() + "' | '" + first + "' '" + second + "' ) " +
//                        "( '" + t.name() + "' " + GET_POS + " " + GET_POS_2 + " | '" + first + "' " + GET_POS +
//                        " '" + second + "' " + GET_POS_2 + " ) ";
                return
                        "( '" + first + "' " + GET_POS +
                        " '" + second + "' " + GET_POS_2 + " ) ";
            }
            case EU:
            case ER:
            case AU:
            case AR:
                throw new RuntimeException("has no simple representation");
            case E_TILDE:
            case A_TILDE:
                throw new RuntimeException("virtual operators do not occur in a grammar");
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case NEG_Oc:
            {
                String o = AbstractOperator.parse(t.name().substring(4)).toString();
                s = "'!' " + GET_POS + " ";
                s += "'" + o + "'";
                return s;
            }
            case NEG_Kc:
            case NEG_Khc:
            {
                String o = AbstractOperator.parse(t.name().substring(4, t.name().length() - 1)).toString();
                s = "'!' " + GET_POS + " ";
                s += "'" + o + "'";
                return s;
            }
            case Eg:
            case Dg:
            case Cg:
            case Oc:
            {
                String o = AbstractOperator.parse(t.name()).toString();
                s += "'" + o + "' " + GET_POS;
                return s;
            }
            case Kc:
            case Khc:
            {
                String o = AbstractOperator.parse(t.name().substring(0, t.name().length() - 1)).toString();
                s += "'" + o + "' " + GET_POS;
                return s;
            }
            case PHI:
            case PSI:
                throw new RuntimeException("no token");
            default:
                throw new RuntimeException("unknown operator");
        }
    }
    private static String pCode(Generator.T t) {
        String sub;
        String coalition = "";
        switch(t) {
            case NEG:
                sub = "subPhi";
                break;
                
            case Kc:
            case Eg:
            case Dg:
            case Cg:
            case Oc:
            case Khc:
            case NEG_Kc:
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case NEG_Oc:
            case NEG_Khc:
                sub = "subSuper";
                coalition = "\n\t\t((UnaryExpression)$result).coalition = $coalition.result;";
                break;
                
            default:
                sub = "sub";
        }
        String p = "";
        switch(t) {
            case FALSE:
                p += "$result = ParserUtils.newPrimary(as, new AsConstant(pos, null, new Literal(false)));";
                break;
            case TRUE:
                p += "$result = ParserUtils.newPrimary(as, new AsConstant(pos, null, new Literal(true)));";
                break;
            case PRIMARY:
                p += "$result = $expr.result;";
                break;
            case NEG:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        "UnaryExpression.Op.CONDITIONAL_NEGATION, $" + sub + ".result);";
                break;
            case AND:
            case OR:
            case U:
            case R:
            case IMPLICATION:
            case EQUIVALENCE:
                throw new RuntimeException("left recursion handled elsewhere");
            case X:
            case F:
            case G:
            case E:
            case A:
            case Eg:
            case Dg:
            case Cg:
            case Oc:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse(t.name())) + ", $" + sub + ".result);";
                break;
            case Kc:
            case Khc:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse(t.name().substring(0, t.name().length() - 1))) + ", $" + sub + ".result);";
                break;
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case NEG_Oc:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse("!" + t.name().substring(4))) + ", $" + sub + ".result);";
                break;
            case NEG_Kc:
            case NEG_Khc:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse("!" + t.name().substring(4, t.name().length() - 1))) + ", $" + sub + ".result);";
                break;
            case EX:
            case EF:
            case EG:
            case AX:
            case AF:
            case AG:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse("" + t.name().charAt(0))) + ", new UnaryExpression(" +
                        "$pos2, $as.SCOPE, " + AbstractOperator.enumId(AbstractOperator.parse("" + t.name().charAt(1))) + ", $" + sub + ".result));";
                break;
            case EU:
            case ER:
            case AU:
            case AR:
                p += "$result = new UnaryExpression($pos, $as.SCOPE, " +
                        AbstractOperator.enumId(AbstractOperator.parse("" + t.name().charAt(0))) + ", new BinaryExpression(" +
                        "$pos2, $as.SCOPE, " + AbstractOperator.enumId(AbstractOperator.parse("" + t.name().charAt(1))) + ", $left.result, $right.result));";
                break;
            case E_TILDE:
                p += "$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.E, $sub.result);";
                break;
            case A_TILDE:
                p += "$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.A, $sub.result);";
                break;
            case PHI:
                p += "$result = $subPhi.result;";
                break;
            case PSI:
                p += "$result = $subPsi.result;";
                break;
            default:
                throw new RuntimeException("unknown operator");
        }
        p += coalition;
        return CompilerUtils.indent(2, p, "\t");
    }
    private static void generateRight(String name, Generator.PT pt,
            boolean parenthesized, PrintWriter out) {
        String p = "";
        switch(pt.t) {
            case FALSE:
            case TRUE:
                throw new RuntimeException("already handled by PRIMARY");
                
            case PRIMARY:
                p = pToken(pt.t);
                break;
                
            case AND:
            case OR:
            case U:
            case R:
            case IMPLICATION:
            case EQUIVALENCE:
                p += "left = property" + pName(name, pt.p) + " " + pToken(pt.t) + " right = property" + pName(name, pt.p);
                break;
            case EU:
            case ER:
            case AU:
            case AR:
            {
                char first = pt.t.toString().charAt(0);
                char second = pt.t.toString().charAt(1);
                String t = "'" + first + "' '(' left = property" +
                        pName(name, pt.p) + " '" + second + "'";
                p +=
                         "'" + first + "' " + GET_POS + " '(' left = property" + pName(name, pt.p) + " '" + second + "' "+
                         GET_POS_2 + " right = property" + pName(name, pt.p) + " ')'";
                break;
            }
            case NEG:
            case X:
            case F:
            case G:
            case EX:
            case EF:
            case EG:
            case AX:
            case AF:
            case AG:
                p += pToken(pt.t) + " sub" + (pt.t == Generator.T.NEG ? "Phi" : ""  ) +
                        " = property" + pName(name, pt.p);
                break;
            case Kc:
            case Eg:
            case Dg:
            case Cg:
            case Oc:
            case Khc:
            case NEG_Kc:
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case NEG_Oc:
            case NEG_Khc:
                    String n = pName(name, pt.p);
                    if(n.contains("Xi")) {
                        n = n.substring(0, n.length() - 5) + "_super[$as]";
                    }
                    p += pToken(pt.t) + " '(' coalition = coalitionGroup[$as] ',' subSuper = property" + n + " ')'";
                break;
            case E:
            case A:
                    p += pToken(pt.t) + " sub = property" + pName(name, pt.p);
                break;
            case E_TILDE:
            case A_TILDE:
                // virtual operators
                p = "sub = property" + pName(name, pt.p);
                break;
            case PHI:
                p = "subPhi = property" + pName(name, pt.p.PHI);
                if(name.contains("A") && parenthesized) {
                    String q = p.substring(0, p.length() - 5) + "_sub";
                    p = q + "[$as]";
                }
                break;
            case PSI:
                if(name.contains("A") && parenthesized)
                    p = pName(name, pt.p.PSI_SUB);
                else
                    p = pName(name, pt.p.PSI);
                p = "subPsi = property" + p;
                break;
            default:
                throw new RuntimeException("unknown operator");
        }
        out.print(p);
    }
    public static boolean leftRecursive(Generator.T t) {
        switch(t) {
            case FALSE:
            case TRUE:
            case PRIMARY:
            case NEG:
            case X:
            case F:
            case G:
            case E:
            case A:
            case EX:
            case EF:
            case EG:
            case AX:
            case AF:
            case AG:
            case EU:
            case ER:
            case AU:
            case AR:
            case E_TILDE:
            case A_TILDE:
            case NEG_Kc:
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case NEG_Oc:
            case NEG_Khc:
            case Kc:
            case Eg:
            case Dg:
            case Cg:
            case Oc:
            case Khc:
            case PHI:
            case PSI:
                return false;
                
            case AND:
            case OR:
            case U:
            case R:
            case IMPLICATION:
            case EQUIVALENCE:
                return true;
                
            default:
                throw new RuntimeException("unknown operator");
        }
    }
    private static Iterable<Generator.PT> terminalsToTail(List<Generator.PT> right) {
        List<Generator.PT> out = new LinkedList<>();
        for(Generator.PT pt : right)
            switch(pt.t) {
                case PRIMARY:
                case TRUE:
                case FALSE:
                    continue;
                default:
                    out.add(pt);
            }
        for(Generator.PT pt : right)
            switch(pt.t) {
                case PRIMARY:
                case TRUE:
                case FALSE:
                    out.add(pt);
                default:
                    continue;
            }
        return out;
    }
    private static String getGrammarPrefix(String name) {
        String SP = GET_POS + " ";
        String s = "'";
        int starred = name.lastIndexOf("A");
        if(starred != -1 && starred != 0) {
            s += name.substring(0, starred) + "' " + SP + "'*";
            SP = "";
            if(starred < name.length() - 1)
                s += "' '" + name.substring(starred + 1);
        } else
            s += name;
        s += "' " + SP + "tName = Identifier ':=' ";
        return s;
    }
    public static void generateProduction(String name, String left, List<Generator.PT> right,
            PrintWriter out, boolean starred, boolean xiSuper) {
        boolean head = name.equals(left);
        if(head) {
            out.println(
                    "/*\n" +
                    " * Grammar of " + name + ".\n" +
                    " */");
            out.println("property" + left + "Statement[AgentSystem as] " +
                "returns [ AsProperty result ]");
            out.println(
                    "locals [\n" +
                    "\tStreamPos pos,\n" +
                    "\tAsProperty out\n" +
                    "]\n:");
            out.println(
                    "" + getGrammarPrefix(name) + " {\n" +
                    "\t$out = new AsProperty($pos, AsProperty.Type." + name + ", $tName.text);\n" +
                    "} expr = property" + left + "[$as] {\n" +
                    "\t$out.EXPR = $expr.result;\n" +
                    "\t$result = $out;\n" +
                    "}");
            out.println(";");
        }
        boolean leftRecursion = false;
        boolean leftRecursionXi = false;
        String recursionXi = "";
        String call = left + "[$as]";
        boolean optionalOpPrefix = false;
        boolean containsPrimary = false;
        boolean declarePos2 = false;
        String lookaheadOps = "";
        for(Generator.PT pt : right)
            if(leftRecursive(pt.t)) {
                String s = pToken(pt.t);
                s = s.substring(0, s.indexOf("{"));
                lookaheadOps += s.trim() + " ";
            }
        for(Generator.PT pt : right) {
            if(leftRecursive(pt.t) && pName(name, pt.p).equals(call)) {
                    leftRecursion = true;
                    switch(pt.t) {
                        case AND:
                        case OR:
                        case IMPLICATION:
                        case EQUIVALENCE:
                            // optionalOpPrefix = true;
                            break;
                        default:
                            /* empty */
                    }
            }
            if(leftRecursive(pt.t) && !pName(name, pt.p).equals(call) && pName(name, pt.p).endsWith("Xi[$as]")) {
                    leftRecursionXi = true;
                    switch(pt.t) {
                        case U:
                        case R:
                            break;
                        default:
                            /* empty */
                    }
            }
            if(pt.t == Generator.T.PRIMARY)
                containsPrimary = true;
            switch(pt.t) {
                case EX:
                case EF:
                case EG:
                case AX:
                case AF:
                case AG:
                case EU:
                case ER:
                case AU:
                case AR:
                    declarePos2 = true;
                default:
                    /* empty */
                    break;
            }
        }
        
        // first pass top, second pass only if left recursion, sub, 
        for(int pass = 0; pass <= 1; ++pass) {
            if(!leftRecursion && pass == 1)
                break;
            String sub;
            out.println("property" +  left +
                    (pass == 1 ? "_sub" : "") +
                    (xiSuper ? "_super" : "") +
                    "[AgentSystem as] " +
                "returns [ AbstractExpression result ]\n" +
                "locals [");
            out.print(
                "\tStreamPos pos,\n" +
                "\tBinaryExpression.Op op");
            if(containsPrimary && (!leftRecursion || pass == 1))
                // out.println("\tAbstractExpression expr;");
                ;
            if(declarePos2)
                out.println(",\n\tStreamPos pos2");
            out.println("\n]\n:");
            boolean first = true;
            if(pass == 1) {
                out.println("\t'(' subParenthesized = property" + left + "[$as] ')' {\n" +
                        "\t\t$result = $subParenthesized.result;");
                out.println("\t}");
                first = false;
            }
            if(pass == 0 && leftRecursion) {
                out.print("\t");
                if(optionalOpPrefix) {
                    out.print("( ");
                }
                out.print("left = property" + left + "_sub[$as] ");
                if(optionalOpPrefix)
                    out.print(")? ");
                out.print("( ( ");
            }
            if(pass == 0 && leftRecursionXi)
                recursionXi = "|\tleftXi = property" + name + "_Xi[$as] ( ( ";
if(left.equals("ACTLA_Xi"))
    left = left;
            boolean firstXi = true;
            for(Generator.PT pt : terminalsToTail(right)) {
                boolean recursionOp = leftRecursive(pt.t) && pName(name, pt.p).equals(call);
                boolean recursionOpXi = !recursionOp &&
                        leftRecursive(pt.t) && pName(name, pt.p).endsWith("Xi[$as]");
                if(    // all go to one
                        !leftRecursion ||
                        // left recursion goes to top pass
                        (pass == 0 && (recursionOp || recursionOpXi)) ||
                        // prefixed go to sub pass
                        (pass == 1 && !recursionOp && !recursionOpXi)) {
                    if(pass == 0 && leftRecursion) {
                        if(recursionOpXi) {
                            if(!firstXi)
                                recursionXi += " | ";
                            recursionXi += pToken(pt.t);
                            firstXi = false;
                        } else {
                            if(!first)
                                out.print(" | ");
                            out.print(pToken(pt.t));
                            first = false;
                        }
                    } else if(
                            // these two are already handled within <code>PRIMARY</code>
                            pt.t != Generator.T.FALSE &&
                            pt.t != Generator.T.TRUE) {
                        if(!first)
                            out.print("|");
                        out.print("\t");
                        generateRight(name, pt, !xiSuper, out);
                        out.print(" {\n" + pCode(pt.t));
                        out.println("\t}");
                        first = false;
                    }
                }
            }
            if(pass == 0 && leftRecursion) {
                out.println(" ) " + GET_POS + " right = property" + left + "_sub[$as] {\n" + RECURSION_CODE + "} )" + (optionalOpPrefix ? "+" : "*") + " {\n" +
                        "\t\t$result = $left.result;");
                out.println("\t}");
                if(optionalOpPrefix) {
                    out.println("|\t" + "sub = property" + left + "_sub[$as] {");
                    out.println(
                            "\t\t$result = $sub.result;\n" +
                            "\t}");
                }
            }
            if(pass == 0 && leftRecursionXi) {
                out.println(recursionXi +
                        " ) " + GET_POS + " rightXi = property" + name + "_Xi[$as] {\n" + RECURSION_CODE_XI + "} )* {\n" +
                        "\t\t$result = $leftXi.result;");
                out.println("\t}");
            }
            out.println(
                ";");
        }
    }
    private static String generateCall(String name) {
        name = name.replaceAll("\\*", "A");
//        return " p = property" + name + "Statement[$as]";
        String n = "property" + name + "Statement";
        return " " + n + "[$as] { $as.addProperty($" + n + ".result); }";
    }
    public static void generateGrammar(String name, PrintWriter out) throws ParseException {
        Generator g = new Generator(name);
        name = name.replaceAll("\\*", "A");
        generateProduction(name, name, g.getPhi(), out, g.starred, false);
        if(!g.getXi().isEmpty()) {
            generateProduction(name, name + "_Xi", g.getXi(), out, g.starred, false);
            if(name.contains("K"))
                generateProduction(name, name + "_Xi", g.getXi(), out, g.starred, true);
        }
        if(!g.getPsi().isEmpty())
            generateProduction(name, name + "_Psi", g.getPsi(), out, g.starred, false);
    }
    /**
     * Createas a file with property grammars.
     * 
     * @param fileName file to create
     */
    public static void generateGrammar(String fileName) {
        try(PrintWriter out = new PrintWriter(fileName)) {
            boolean gs = Generator.USE_SYMBOLS;
            Generator.USE_SYMBOLS = false;
            out.println(
                "/* THIS FILE IS MACHINE GENERATED */\n\n" +
                "grammar Property;\n\n" +
                "/**\n" +
                " * A single property.\n" +
                " */\n" +
                "property[AgentSystem as]\n" +
                ":\n" +
                "'property' (");
            boolean first = true;
            for(String n : Generator.PREFIXES) {
                out.print("\t");
                if(!first)
                    out.print("|");
                out.print("\t");
                out.println(generateCall(n + ""));
                out.println("\t|\t" + generateCall(n + "K"));
                out.println("\t|\t" + generateCall(n + "KD"));
                first = false;
            }
            out.println(
                ") ';'\n" +
                ";\n");
            for(String n : Generator.PREFIXES) {
                generateGrammar(n + "", out);
                generateGrammar(n + "K", out);
                generateGrammar(n + "KD", out);
            }
        } catch(IOException e) {
            throw new RuntimeException("unexpected: " + e.getMessage());
        } catch(ParseException e) {
            throw new RuntimeException("unexpected: " + e.getMessage());
        }
    }
    public static void main(String[] args) {
        String[] args_ = { "test.jj.property" };
        // args = args_;
        generateGrammar(args[0]);
    }
}
