/*
 * Generator.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent.property;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;

/**
 * Generates grammars for different logics, from LTL to CTL*KD.
 * 
 * @author Artur Rataj
 */
public class Generator {
    /**
     * Must be false for compiler tests.
     */
    public static boolean USE_SYMBOLS = false;
    /**
     * Prefixes of logics available. Suffixes possible <code>K</code> and <code>KD</code>.
     */
    public final static String[] PREFIXES = {
        "LTL", "ACTL", "ACTL*", "ELTL", "ECTL", "ECTL*", "CTL*",
    };
    /**
     * Possible constructs.
     */
    public enum T {
        FALSE, TRUE,
        PRIMARY, 
        NEG, AND, OR,
        X, F, G, U, R,
        IMPLICATION, EQUIVALENCE,
        EX, EF, EG, EU, ER,
        AX, AF, AG, AU, AR,
        E, A,
        E_TILDE, A_TILDE,
        Kc, Eg, Dg, Cg,
        NEG_Kc, NEG_Eg, NEG_Dg, NEG_Cg,
        Oc, Khc, NEG_Oc, NEG_Khc,
        PHI, PSI;
        
        public String toString() {
            String s = super.toString();
            switch(this) {
                case FALSE:
                case TRUE:
                    if(USE_SYMBOLS)
                        return s.toLowerCase();
                    else
                        return s;
                case PRIMARY:
                    return (USE_SYMBOLS ? "p" : s);
                case NEG:
                    return (USE_SYMBOLS ? "¬" : "!");
                case E_TILDE:
                    if(USE_SYMBOLS)
                        return "Ẽ";
                    else
                        return  s.charAt(0) + "~";
                case A_TILDE:
                    if(USE_SYMBOLS)
                        return "Ã";
                    else
                        return  s.charAt(0) + "~";
                case NEG_Kc:
                case NEG_Eg:
                case NEG_Dg:
                case NEG_Cg:
                case NEG_Oc:
                case NEG_Khc:
                    return (USE_SYMBOLS ? "¬" : "!") + s.substring(4);
                case PHI:
                    return (USE_SYMBOLS ? "ɸ" : s.toString());
                case PSI:
                    return (USE_SYMBOLS ? "ψ" : s.toString());
                default:
                    return s;
            }
        }
    };
    public enum P {
        PHI, XI, PSI, PSI_SUB;
        
        public String toString() {
            if(USE_SYMBOLS) {
                switch(this) {
                    case PHI:
                        return "ɸ";
                    case XI:
                        return "ξ";
                    case PSI:
                        return "ψ";
                    default:
                        throw new RuntimeException("unknown symbol representation");
                }
            } else
                return super.toString();
        }
    };
    public static class PT implements Cloneable {
        public T t = null;
        public P p = null;
        
        @Override
        public PT clone() {
            PT copy;
            try {
                copy = (PT)super.clone();
            } catch (CloneNotSupportedException ex) {
                throw new RuntimeException("unexpected");
            }
            copy.t = t;
            copy.p = p;
            return copy;
        }
        @Override
        public String toString() {
            final String SPACE = USE_SYMBOLS ? "" : " ";
            String tS = t.toString();
            String pS;
            if(p != null)
                pS = p.toString();
            else
                pS = "";
            switch(t) {
                case NEG:
                    if(USE_SYMBOLS)
                        return "¬" + pS;
                    else
                        return "!" + pS;
                case AND:
                    if(USE_SYMBOLS)
                        return pS + "∧" + pS;
                    else
                        return pS + "&&" + pS;
                case OR:
                    if(USE_SYMBOLS)
                        return pS + "∨" + pS;
                    else
                        return pS + "||" + pS;
                case U:
                case R:
                    return pS + SPACE + tS + SPACE + pS;
                case IMPLICATION:
                        return pS + "->" + pS;
                case EQUIVALENCE:
                        return pS + "<->" + pS;
                case EU:
                case ER:
                case AU:
                case AR:
                    return tS.charAt(0) + "(" + pS + SPACE + tS.charAt(1) + SPACE + pS + ")";
                    
                default:
                    String s = tS;
                    if(p != null)
                        s += SPACE + pS;
                    return s;
            }
        }
    }
    static List<PT> PRIMITIVE = new LinkedList<>();
    static List<PT> LOGICAL_IE_PHI = new LinkedList<>();
    static List<PT> LOGICAL_IE_XI = new LinkedList<>();
    static List<PT> AND_OR_IE_PSI = new LinkedList<>();
    static List<PT> TEMPORAL_XI = new LinkedList<>();
    static List<PT> TEMPORAL_A_PHI = new LinkedList<>();
    static List<PT> TEMPORAL_E_PHI = new LinkedList<>();
    static List<PT> OP_K_PHI = new LinkedList<>();
    static List<PT> OP_K_XI = new LinkedList<>();
    static List<PT> OP_NEG_K_PHI = new LinkedList<>();
    static List<PT> OP_NEG_K_XI = new LinkedList<>();
    static List<PT> OP_D_PHI = new LinkedList<>();
    static List<PT> OP_D_XI = new LinkedList<>();
    static List<PT> OP_NEG_D_PHI = new LinkedList<>();
    static List<PT> OP_NEG_D_XI = new LinkedList<>();
    static List<PT> PHI_PSI = new LinkedList<>();
    static List<PT> E_XI = new LinkedList<>();
    static List<PT> A_XI = new LinkedList<>();
    static List<PT> E_TILDE_XI = new LinkedList<>();
    static List<PT> A_TILDE_XI = new LinkedList<>();
    static void add(List<PT> op, T from, T to, P p) {
        boolean begin = false;
        boolean end = false;
        for(T t : T.values()) {
            if(t == from)
                begin = true;
            if(begin && !end) {
                PT pt = new PT();
                pt.t = t;
                switch(t) {
                    case FALSE:
                    case TRUE:
                    case PRIMARY:
                        if(p != null)
                            throw new RuntimeException("no argument expected");
                    default:
                        pt.p = p;
                }
                op.add(pt);
            }
            if(t == to)
                end = true;
        }
    }
    static {
        add(PRIMITIVE, T.FALSE, T.PRIMARY, null);
        add(LOGICAL_IE_PHI, T.NEG, T.OR, P.PHI);
        add(LOGICAL_IE_PHI, T.IMPLICATION, T.EQUIVALENCE, P.PHI);
        add(LOGICAL_IE_XI, T.NEG, T.OR, P.XI);
        add(LOGICAL_IE_XI, T.IMPLICATION, T.EQUIVALENCE, P.XI);
        add(AND_OR_IE_PSI, T.AND, T.OR, P.PSI);
        add(AND_OR_IE_PSI, T.IMPLICATION, T.EQUIVALENCE, P.PSI);
        add(TEMPORAL_XI, T.X, T.R, P.XI);
        add(TEMPORAL_A_PHI, T.AX, T.AR, P.PHI);
        add(TEMPORAL_E_PHI, T.EX, T.ER, P.PHI);
        add(OP_K_PHI, T.Kc, T.Cg, P.PHI);
        add(OP_K_XI, T.Kc, T.Cg, P.XI);
        add(OP_NEG_K_PHI, T.NEG_Kc, T.NEG_Cg, P.PHI);
        add(OP_NEG_K_XI, T.NEG_Kc, T.NEG_Cg, P.XI);
        add(OP_D_PHI, T.Oc, T.Khc, P.PHI);
        add(OP_D_XI, T.Oc, T.Khc, P.XI);
        add(OP_NEG_D_PHI, T.NEG_Oc, T.NEG_Khc, P.PHI);
        add(OP_NEG_D_XI, T.NEG_Oc, T.NEG_Khc, P.XI);
        add(PHI_PSI, T.PHI, T.PSI, null);
        add(E_XI, T.E, T.E, P.XI);
        add(A_XI, T.A, T.A, P.XI);
        add(E_TILDE_XI, T.E_TILDE, T.E_TILDE, P.XI);
        add(A_TILDE_XI, T.A_TILDE, T.A_TILDE, P.XI);
    }
    static List<PT> XI_CTLA;
    static List<PT> PSI_CTLA;
    
    /**
     * Name of this logic.
     */
    final public String NAME;
    /**
     * Base, i.e. without the starred and KD parts.
     */
    public String base = null;
    /**
     * If starred.
     */
    public boolean starred = false;
    /**
     * If E or EA.
     */
    public  boolean E = false;
    /**
     * If A or EA.
     */
    public  boolean A = false;
    /**
     * If K or KD.
     */
    public  boolean K = false;
    /**
     * If KD.
     */
    boolean D = false;
    
    public Generator(String name) throws ParseException {
        if(XI_CTLA == null) {
            XI_CTLA = new LinkedList<>();
            PSI_CTLA = new LinkedList<>();
            Generator g = new Generator("CTL*");
            XI_CTLA.addAll(g.getXi());
            PSI_CTLA.addAll(g.getPsi());
        }
        NAME = name;
        if(name.startsWith("LTL")) {
            base = "LTL";
            A = true;
        } else if(name.startsWith("ACTL")) {
            base = "ACTL";
            A = true;
        } else if(name.startsWith("ELTL")) {
            base = "ELTL";
            E = true;
        } else if(name.startsWith("ECTL")) {
            base = "ECTL";
            E = true;
        } else if(name.startsWith("CTL")) {
            base = "CTL";
            A = true;
            E = true;
        }
        getSKD();
        if(starred && (base.equals("LTL") || base.equals("ELTL")))
            throw new ParseException(null, ParseException.Code.ILLEGAL,
                    base + "--type logic can not be starred");
        if(!starred && base.equals("CTL"))
            throw new ParseException(null, ParseException.Code.ILLEGAL,
                    base + "--type logic must be starred");
    }
    /**
     * Sets starred, K and D.
     */
    private void getSKD() throws ParseException {
        int baseLength = base.length();
        if(NAME.length() > baseLength &&
                NAME.charAt(baseLength) == '*')
            starred = true;
        if(starred)
            ++baseLength;
        String suffix = NAME.substring(baseLength);
        if(suffix.isEmpty()) {
            /* empty */
        } else if(suffix.equals("K")) {
            K = true;
        } else if(suffix.equals("KD")) {
            K = true;
            D = true;
        } else
            throw new ParseException(null, ParseException.Code.ILLEGAL,
                        "invalid suffix");
    }
    public List<PT> getXi() {
        List<PT> out = new LinkedList<>();
        if(base.equals("LTL") || base.equals("ELTL")) {
            boolean ltl = base.equals("LTL");
            out.addAll(PRIMITIVE);
            out.addAll(LOGICAL_IE_XI);
            out.addAll(TEMPORAL_XI);
            if(K) {
                if(ltl)
                    out.addAll(OP_K_XI);
                out.addAll(OP_NEG_K_XI);
            }
            if(D) {
                if(ltl)
                    out.addAll(OP_D_XI);
                out.addAll(OP_NEG_D_XI);
            }
        } else if(base.equals("ACTL") || base.equals("ECTL")) {
            if(starred) {
                out.addAll(XI_CTLA);
            } else {
                /* empty */
            }
        } else if(base.equals("CTL")) {
            out.addAll(PHI_PSI);
        } else
            throw new RuntimeException("unknown base");
        return out;
    }
    public List<PT> getPsi() {
        List<PT> out = new LinkedList<>();
        if(!starred) {
            /* empty */
        } else if(base.equals("ACTL") || base.equals("ECTL")) {
            out.addAll(PSI_CTLA);
        } else if(base.equals("CTL")) {
            out.addAll(AND_OR_IE_PSI);
            out.addAll(TEMPORAL_XI);
        } else
            throw new RuntimeException("unknown base");
        return out;
    }
    public List<PT> getPhi() {
        List<PT> out = new LinkedList<>();
        if(base.equals("LTL")) {
            out.addAll(A_TILDE_XI);
        } else if(base.equals("ELTL")) {
            out.addAll(E_TILDE_XI);
        } else if(base.equals("ACTL")) {
            out.addAll(PRIMITIVE);
            out.addAll(LOGICAL_IE_PHI);
            if(starred) {
                out.addAll(A_XI);
                if(K)
                    out.addAll(OP_K_XI);
                if(D)
                    out.addAll(OP_D_XI);
            } else {
                out.addAll(TEMPORAL_A_PHI);
                if(K)
                    out.addAll(OP_K_PHI);
                if(D)
                    out.addAll(OP_D_PHI);
            }
        } else if(base.equals("ECTL")) {
            out.addAll(PRIMITIVE);
            out.addAll(LOGICAL_IE_PHI);
            if(starred) {
                out.addAll(E_XI);
                if(K)
                    out.addAll(OP_NEG_K_XI);
                if(D)
                    out.addAll(OP_NEG_D_XI);
            } else {
                out.addAll(TEMPORAL_E_PHI);
                if(K)
                    out.addAll(OP_NEG_K_PHI);
                if(D)
                    out.addAll(OP_NEG_D_PHI);
            }
        } else if(base.equals("CTL")) {
            out.addAll(PRIMITIVE);
            out.addAll(LOGICAL_IE_PHI);
            out.addAll(E_XI);
            out.addAll(A_XI);
            if(starred) {
                if(K) {
                    out.addAll(OP_K_XI);
                    out.addAll(OP_NEG_K_XI);
                }
                if(D) {
                    out.addAll(OP_D_XI);
                    out.addAll(OP_NEG_D_XI);
                }
            } else {
                if(K) {
                    out.addAll(OP_K_PHI);
                    out.addAll(OP_NEG_K_PHI);
                }
                if(D) {
                    out.addAll(OP_D_PHI);
                    out.addAll(OP_NEG_D_PHI);
                }
            }
        } else
            throw new RuntimeException("unknown base");
        return out;
    }
    public static String toString(List<PT> list) {
        final String SPACE = USE_SYMBOLS ? "" : " ";
        StringBuilder out = new StringBuilder();
        boolean first = true;
        for(PT pt : list) {
            if(!first)
                out.append(SPACE + "|" + SPACE);
            out.append(pt.toString());
            first = false;
        }
        return out.toString();
    }
    public static void test(String name, String phi, String xi, String psi)
            throws ParseException {
        Generator g = new Generator(name);
        String gPhi = toString(g.getPhi());
        String gXi = toString(g.getXi());
        String gPsi = toString(g.getPsi());
        if(phi.isEmpty()) {
            if(USE_SYMBOLS) {
                System.out.println(name + "\n\t" +
                        "ɸ ::= " + gPhi +
                        (gXi.isEmpty() ? "" : "\n\tξ ::= " + gXi) +
                        (gPsi.isEmpty() ? "" : "\n\tψ ::= " + gPsi));
            } else {
                System.out.println("test(\"" + name + "\",\n\t" +
                        "\"" + gPhi + "\",\n\t" +
                        "\"" + gXi + "\",\n\t" +
                        "\"" + gPsi + "\");");
            }
        } else {
            System.out.println("\tlogic " + name + "\n\tpattern " +
                    phi + ", " + xi + ", " + psi);
            if(!gPhi.equals(phi))
                throw new ParseException(null, ParseException.Code.INVALID,
                        "phi differs, is\n\t" + gPhi + "\nexpected\n\t" + phi);
            if(!gXi.equals(xi))
                throw new ParseException(null, ParseException.Code.INVALID,
                        "xi differs, is\n\t" + gXi + "\nexpected\n\t" + xi);
            if(!gPsi.equals(psi))
                throw new ParseException(null, ParseException.Code.INVALID,
                        "psi differs, is\n\t" + gPsi + "\nexpected\n\t" + psi);
        }
    }
    /**
     * Tests this generator.
     */
    public static void test() throws ParseException {
        boolean gs = Generator.USE_SYMBOLS;
        Generator.USE_SYMBOLS = false;
        test("LTL",
                "A~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI",
                "");
        test("LTLK",
                "A~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI | Kc XI | Eg XI | Dg XI | Cg XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI",
                "");
        test("LTLKD",
                "A~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI | Kc XI | Eg XI | Dg XI | Cg XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI | Oc XI | Khc XI | !Oc XI | !Khc XI",
                "");
        test("ACTL",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | AX PHI | AF PHI | AG PHI | A(PHI U PHI) | A(PHI R PHI)",
                "",
                "");
        test("ACTLK",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | AX PHI | AF PHI | AG PHI | A(PHI U PHI) | A(PHI R PHI) | Kc PHI | Eg PHI | Dg PHI | Cg PHI",
                "",
                "");
        test("ACTLKD",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | AX PHI | AF PHI | AG PHI | A(PHI U PHI) | A(PHI R PHI) | Kc PHI | Eg PHI | Dg PHI | Cg PHI | Oc PHI | Khc PHI",
                "",
                "");
        test("ACTL*",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | A XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("ACTL*K",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | A XI | Kc XI | Eg XI | Dg XI | Cg XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("ACTL*KD",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | A XI | Kc XI | Eg XI | Dg XI | Cg XI | Oc XI | Khc XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("ELTL",
                "E~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI",
                "");
        test("ELTLK",
                "E~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI",
                "");
        test("ELTLKD",
                "E~ XI",
                "FALSE | TRUE | PRIMARY | !XI | XI&&XI | XI||XI | XI->XI | XI<->XI | X XI | F XI | G XI | XI U XI | XI R XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI | !Oc XI | !Khc XI",
                "");
        test("ECTL",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | EX PHI | EF PHI | EG PHI | E(PHI U PHI) | E(PHI R PHI)",
                "",
                "");
        test("ECTLK",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | EX PHI | EF PHI | EG PHI | E(PHI U PHI) | E(PHI R PHI) | !Kc PHI | !Eg PHI | !Dg PHI | !Cg PHI",
                "",
                "");
        test("ECTLKD",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | EX PHI | EF PHI | EG PHI | E(PHI U PHI) | E(PHI R PHI) | !Kc PHI | !Eg PHI | !Dg PHI | !Cg PHI | !Oc PHI | !Khc PHI",
                "",
                "");
        test("ECTL*",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("ECTL*K",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("ECTL*KD",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI | !Oc XI | !Khc XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("CTL*",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI | A XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("CTL*K",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI | A XI | Kc XI | Eg XI | Dg XI | Cg XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        test("CTL*KD",
                "FALSE | TRUE | PRIMARY | !PHI | PHI&&PHI | PHI||PHI | PHI->PHI | PHI<->PHI | E XI | A XI | Kc XI | Eg XI | Dg XI | Cg XI | !Kc XI | !Eg XI | !Dg XI | !Cg XI | Oc XI | Khc XI | !Oc XI | !Khc XI",
                "PHI | PSI",
                "PSI&&PSI | PSI||PSI | PSI->PSI | PSI<->PSI | X XI | F XI | G XI | XI U XI | XI R XI");
        Generator.USE_SYMBOLS = gs;
    }
    public static void printWholeSyntax() throws ParseException {
        System.out.println("Parenthesization and weighted variants " +
                "not shown for brevity. E implies !A, A implies !E.");
        for(String n : PREFIXES) {
            test(n + "", "", "", "");
            test(n + "K", "", "", "");
            test(n + "KD", "", "", "");
        }
    }
    public static void main(String[] args) throws ParseException {
        USE_SYMBOLS = false;
        printWholeSyntax();
    }
}
