/*
 * AsConstant.java
 *
 * Created on Sep 23, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A constant, integer or double or boolean.
 * 
 * @author Artur Rataj
 */
public class AsConstant extends Literal implements Cloneable, Comparable, SourcePosition, AsTyped {
    /**
     * Position of this variable in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * Name of this value, null for none.
     */
    public final String NAME;
    /**
     * Creates a new integer constant.
     * 
     * @param pos position in the parsed stream
     * @param name name of the constant, null for none
     * @param value value of this constant
     */
    public AsConstant(StreamPos pos, String name, Literal value) {
        super(value);
        setStreamPos(pos);
        NAME = name;
    }
    /**
     * Creates a name holder for a constant.
     * 
     * @param pos position of the holder in the input stream
     * @param name name of the constant, null for none
     */
    public AsConstant(StreamPos pos, String name) {
        super(new Literal());
        setStreamPos(pos);
        NAME = name;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public boolean isFloating() {
        return type.isOfFloatingPointTypes();
    }
    @Override
    public boolean isBoolean() {
        return type.isBoolean();
    }
    /**
     * Returns an 
     * @return 
     */
    @Override
    public String getTypeId() {
        return AsTyped.getTypeId(isFloating(), isBoolean());
    }
    public String getValueStr() {
        switch(getTypeId()) {
            case "B":
                return "" + getBoolean();
                
            case "R":
                return "" + getMaxPrecisionFloatingPoint();
                
            case "Z":
                return "" + getMaxPrecisionInteger();
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    @Override
    public int compareTo(Object o) {
        AsConstant other = (AsConstant)o;
        if(isBoolean() && other.isBoolean()) {
            if(!getBoolean() && other.getBoolean())
                return -1;
            else if(getBoolean() && !other.getBoolean())
                return 1;
            else
                return 0;
        } else if(!isBoolean() && !other.isBoolean()) {
            if(getMaxPrecisionFloatingPoint() < other.getMaxPrecisionFloatingPoint())
                return -1;
            else if(getMaxPrecisionFloatingPoint() > other.getMaxPrecisionFloatingPoint())
                return 1;
            else
                return 0;
        } else
            throw new RuntimeException("mixed boolean and arithmetic values");
    }
    @Override
    public boolean equals(Object o) {
        return compareTo(o) == 0;
    }
    @Override
    public AsConstant clone() {
        AsConstant copy;
        copy = (AsConstant)super.clone();
        return copy;
    }
    @Override
    public String toString() {
        return (NAME == null  ? "" : NAME + " = ") + super.toString();
    }
}
