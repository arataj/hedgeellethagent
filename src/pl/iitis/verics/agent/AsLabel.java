/*
 * AsLabel.java
 *
 * Created on Oct 14, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A synchronising label.
 * 
 * @author Artur Rataj
 */
public class AsLabel implements Comparable, SourcePosition, AsTyped {
    /**
     * A sync key of a null label. It does not sync to anything, even if it reappears
     * in different modules.
     */
    public final static String SYNC_KEY_NONE = "none";
    /**
     * Position of this variable in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * Name of this label, unqualified.
     */
    public final String NAME;

    /**
     * Creates a new statement.
     * 
     * @param pos position in the source
     */
    public AsLabel(StreamPos pos, String name) {
        setStreamPos(pos);
        NAME = name;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public boolean isFloating() {
        return false;
    }
    @Override
    public boolean isBoolean() {
        return true;
    }
    @Override
    public String getTypeId() {
        return AsTyped.getTypeId(isFloating(), isBoolean());
    }
    @Override
    public int compareTo(Object o) {
        AsLabel other = (AsLabel)o;
        return NAME.compareTo(other.NAME);
    }
    /**
     * Returns the syncing key of this label.
     * 
     * @param label label, null for no action
     * @return a key
     */
    public static String getKey(AsLabel label) {
        if(label == null)
            return SYNC_KEY_NONE;
        else
            return label.NAME;
    }
    @Override
    public String toString() {
        return "[" + NAME + "]";
    }
}
