/*
 * AsFormula.java
 *
 * Created on Sep 24, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A formula.
 * 
 * @author Artur Rataj
 */
public class AsFormula extends AbstractNamedExpression {
    /**
     * Creates a new definition.
     * 
     * @param pos position in the source stream
     * @param name name
     * @param expr expression
     */
    public AsFormula(StreamPos pos, String name, AbstractExpression expr) {
        super(pos, name, expr);
    }
}
