/*
 * AgentSystem.java
 *
 * Created on Sep 23, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.LocalFlags;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.MethodScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.iitis.verics.agent.property.AsProperty;

/**
 * A system of agents.
 * 
 * @author Artur Rataj
 */
public class AgentSystem {
    /**
     * Defines possible types of this agent system.
     */
    public static enum Type {
        /* both fully observable and POMDP */
        MDP;
        
        /**
         * Sets a type represented by a string. If not recognized,
         * an MDP type is set regardless, but a parse exception is thrown.
         * 
         * @param s textual representation
         * @param as writes to the system's <code>type</code>, <code>fullyObservable</code>
         */
        public static void parseType(String s, AgentSystem as) throws ParseException {
            switch(s) {
                case "mdp":
                    as.type = Type.MDP;
                    as.fullyObservable = true;
                    break;
                    
                case "pomdp":
                    as.type = Type.MDP;
                    as.fullyObservable = false;
                    break;
                    
                default:
                    // just for the parser to go on
                    as.type = Type.MDP;
                    as.fullyObservable = true;
                    throw new ParseException(null,
                        ParseException.Code.UNKNOWN,
                        "unknown model type `" + s + "'");
            }
        }
    }
    /**
     * A name of this system.
     */
    final public String NAME;
    /**
     * Type of this agent system.
     */
    public Type type = null;
    /**
     * If fully observable. The default is <code>true</code>.
     */
    public boolean fullyObservable = true;
    /**
     * If synchronous. The default is <code>false</code>.
     */
    public boolean synchronous = false;
    /**
     * Global constants.
     */
    public List<AsConstant> constantList = new ArrayList<>();
    /**
     * Global constants, keyed with names.
     */
    protected SortedMap<String, AsConstant> constants = new TreeMap<>();
    /**
     * Modules. The currently parsed module should be the last one on this list,
     * so that <code>lookupValue()</code> works correctly.
     */
    public List<AsModule> moduleList = new ArrayList<>();
    /**
     * Modules, keyed with names.
     */
    protected SortedMap<String, AsModule> modules = new TreeMap<>();
    /**
     * Formulas.
     */
    public List<AsFormula> formulaList = new ArrayList<>();
    /**
     * Formulas, keyed with names.
     */
    protected SortedMap<String, AsFormula> formulas = new TreeMap<>();
    /**
     * Properties.
     */
    public List<AsProperty> propertyList = new ArrayList<>();
    /**
     * Properties, keyed with names.
     */
    protected SortedMap<String, AsProperty> properties = new TreeMap<>();
    /**
     * Expressions registered using <code>register(AbstractExpression, AsFormula)</code>.
     */
    protected Map<AbstractExpression, AsFormula> formulaReplacements;
    /**
     * A filter on initial states, superimposed on the cartesian product of sets of
     * initial values of each state variable.
     */
    public AbstractExpression initFilter;
    /**
     * A common, fake scope for Verics expression.
     */
    public BlockScope SCOPE;
    /**
     * A cache variable, to avoid multiple instances of the same equivalent of some
     * <code>AsVariable</code>.
     */
    public Map<String, Variable> variableCache;
    /**
     * <p>If the method <code>toString()</code> of an expression shadow
     * <code>As*Expression</code> should return formula name, if
     * replaces it.</p>
     * 
     * <p>The default is false.</p>
     */
    public boolean toStringRestoreFormulaEnabled;
    /**
     * If within a parsed module, i.e. local variables can be looked up.
     */
    public boolean inModule = false;
    
    /**
     * Creates a new agent system, with <code>initFilter</code> being
     * a constant expression of true.
     * 
     * @param name name of this system
     */
    public AgentSystem(String name) {
        NAME = name;
        createFakeScope();
        initFilter = new ConstantExpression(null, SCOPE, new Literal(true));
        formulaReplacements = new HashMap<>();
        variableCache = new HashMap<>();
        toStringRestoreFormulaEnabled = false;
    }
    private void createFakeScope() {
        MethodSignature s = new MethodSignature(null, "fake", new LinkedList<>());
        Method m = new Method(new MethodSignature(null, s.extractLocalString()).toString());
        m.signature = s;
        MethodScope mScope = new MethodScope(m, null, m.signature.toString());
        SCOPE = new BlockScope(m, mScope, "fake");
        SCOPE.parent = null;
    }
    /**
     * Adds a global constant to this system.
     * 
     * @param constant a named constant
     */
    public void addConstant(AsConstant constant) {
        if(constant.NAME == null)
            throw new RuntimeException("a global constant must be named");
        constantList.add(constant);
        constants.put(constant.NAME, constant);
    }
    /**
     * Replaces a global constant in this system.
     * 
     * @param prev constant to be replaced, should new newly declared so that
     * no references to it exist yet
     * @param curr the replacing constant
     */
    public void replaceConstant(AsConstant prev, AsConstant curr) {
        if(curr.NAME == null)
            throw new RuntimeException("a global constant must be named");
        if(constantList.indexOf(prev) != constantList.size() - 1)
            throw new RuntimeException("no such constant or no latest declared");
        constantList.remove(prev);
        constants.remove(prev.NAME);
        addConstant(curr);
    }
    /**
     * Returns a global constant by its name.
     * 
     * @param name name of the constant to look up
     * @return a constant or null if not found
     */
    public AsConstant lookupConstant(String name) {
        return constants.get(name);
    }
    /**
     * Adds a module to this system.
     * 
     * @param module a module to add
     */
    public void addModule(AsModule module) {
        moduleList.add(module);
        modules.put(module.NAME, module);
    }
    /**
     * Returns a module by its name.
     * 
     * @param name name of the module to look up
     * @return a module or null if not found
     */
    public AsModule lookupModule(String name) {
        return modules.get(name);
    }
    /**
     * Adds a formula to this system.
     * 
     * @param formula a formula to add
     */
    public void addFormula(AsFormula formula) {
        formulaList.add(formula);
        formulas.put(formula.NAME, formula);
    }
    /**
     * Returns a formula by its name.
     * 
     * @param name name of the formula to look up
     * @return a formula or null if not found
     */
    public AsFormula lookupFormula(String name) {
        return formulas.get(name);
    }
    /**
     * Adds a property to this system.
     * 
     * @param property a property to add
     */
    public void addProperty(AsProperty property) {
        propertyList.add(property);
        properties.put(property.NAME, property);
    }
    /**
     * Returns a formula by its name.
     * 
     * @param name name of the formula to look up
     * @return a formula or null if not found
     */
    public AsProperty lookupProperty(String name) {
        return properties.get(name);
    }
    public AsModule getParsedModule() {
        if(!inModule)
            throw new RuntimeException("not in a module");
        return moduleList.get(moduleList.size() - 1);
    }
    /**
     * Returns a global constant or a module variable.
     * 
     * @param name name of the value to return; if variable,
     * then outside the parser must be qualified
     * @return a value or null of not found
     */
    public AsTyped lookupValue(String name) {
        int pos = name.indexOf('.');
        if(pos == -1) {
            if(!inModule)
                return constants.get(name);
            else {
                AsTyped t = getParsedModule().lookupVariable(name);
                if(t != null)
                    // local variables have precedence to global constants
                    return t;
                else
                    return constants.get(name);
            }
        } else {
            String m = name.substring(0, pos);
            AsModule module = lookupModule(m);
            if(module == null)
                return null;
            else
                return module.lookupVariable(name.substring(pos + 1));
        }
    }
    /**
     * Returns the action experssion associated with a qualified
     * protocol action.
     * 
     * @param pos position in the source stream
     * @param name name of the action
     * @return expression or null if action not found; no source position
     */
    public AsActionExpression getActionExpression(StreamPos position, String name) {
        int pos = name.indexOf('.');
        if(pos == -1)
            throw new RuntimeException("qualified name expected");
        String moduleName = name.substring(0, pos);
        name = name.substring(pos + 1);
        AsModule module = lookupModule(moduleName);
        if(module != null) {
            if(module.PROTOCOL.map.containsKey(name)) {
                return new AsActionExpression(this, position, this.SCOPE, module,
                    name);
            }
        }
        return null;
    }
    /**
     * <p>Registers a given expression as a replacement of a formula.</p>
     * 
     * <p>The register is used to restore the formula, if required.</p>
     * 
     * @param expr expression, which has replaced a formula
     * @param formula the formula replaced
     */
    public void register(AbstractExpression expr, AsFormula formula) {
        formulaReplacements.put(expr, formula);
    }
    /**
     * If a given expression replaces a formula.
     * 
     * @param expr expression to test
     * @return the replaced formula, or null if the expression does not
     * replace any formula
     */
    public AsFormula isReplaced(AbstractExpression expr) {
        return formulaReplacements.get(expr);
    }
    /**
     * Creates, or gets from cache, a Hedgelleth equivalent of <code>AsVariable</code>.
     * Thanks to the cache, multiple instances of <code>AsVariable</code>s having a
     * common owner and name are avoided.
     * 
     * @param v source variable
     * @return a Hedgeelleth--style variable
     */
    public Variable toVariable(AsVariable v) {
        String key = v.getQualifiedName();
        Variable hv = variableCache.get(key);
        // System.out.println("asked " + key);
        if(hv == null) {
            // System.out.println("not in cache");
            hv = v.toVariable();
            variableCache.put(key, hv);
        }
        return hv;
    }
    /**
     * Called to complete its elements, lice access right. This
     * system must be already constructed.
     */
    public void completeParse() throws ParseException {
        ParseException error = new ParseException();
        for(AsModule m : moduleList) {
            for(AsVariable v : m.variableList) {
                try {
                    v.completeParse(this);
                    if(m.FORCE_PRIVATE_VARIABLES && !v.COMMON.PRIVATE)
                        throw new ParseException(v.COMMON.pos, ParseException.Code.ILLEGAL,
                                "only private variables allowed in this module");
                } catch(ParseException e) {
                    error.addAllReports(e);
                }
            }
            for(List<String> l : m.PROTOCOL.actions)
                for(String s : l)
                    m.ACTIONS.add(s);
        }
        if(error.reportsExist())
            throw error;
    }
    /**
     * Creates, or gets from cache, a Hedgelleth equivalent of a global constant.
     * Thanks to the cache, multiple instances of <code>AsConstant</code>s having a
     * common name are avoided.
     * 
     * @param v source global constant
     * @return a Hedgeelleth--style variable
     */
    public Variable toVariable(AsConstant c) {
        String key = c.NAME;
        Variable hv = variableCache.get(key);
        if(hv == null) {
            hv = new Variable(c.getStreamPos(), null,
                    c.type, c.NAME);
            hv.flags = new LocalFlags();
            variableCache.put(key, hv);
        }
        return hv;
    }
    /**
     * Returns global constants as Verics--style variables.
     * 
     * @return a collection of named constants.
     */
    public Map<Variable, AsConstant> getKnowns() {
        Map<Variable, AsConstant> map = new HashMap<>();
        for(AsConstant c : constantList)
            map.put(toVariable(c), c);
        return map;
    }
    /**
     * Returns state variables as Verics--style variables.
     * 
     * @return a collection of variables
     */
    public Set<Variable> getUnknowns() {
        Set<Variable> set = new HashSet<>();
        for(AsModule m : moduleList) {
            for(AsVariable sv : m.variableList) {
                set.add(toVariable(sv));
            }
        }
        return set;
    }
    /**
     * Returns all statements which have a given label of a given name.
     * 
     * @param label the label
     * @param exclude if not null, excludes all statements from this module
     * @return list of statements, in the order of definition
     */
    public List<AsStatement> getSynced(AsLabel label, AsModule exclude) {
        List<AsStatement> out = new ArrayList<>();
        if(label != null)
            for(AsModule m : moduleList)
                if(exclude == null || m != exclude)
                    for(AsStatement s : m.STATEMENTS)
                        if(AsLabel.getKey(label).equals(AsLabel.getKey(s.LABEL)))
                            out.add(s);
        return out;
    }
}
