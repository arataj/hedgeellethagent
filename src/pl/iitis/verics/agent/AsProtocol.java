/*
 * AsProtocol.java
 *
 * Created on Mar 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Protocol specification.
 * 
 * @author Artur Rataj
 */
public class AsProtocol implements SourcePosition {
    /**
     * Position of this protocol in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * The owner of this protocol.
     */
    public final AsModule OWNER;
    /**
     * Subsequent guards.
     */
    public final List<AbstractExpression> guards;
    /**
     * Subsequent actions.
     */
    public final List<List<String>> actions;
    /**
     * Maps an action to its guard.
     */
    public final SortedMap<String, List<AbstractExpression>> map;
            
    /**
     * Creates a new protocol.
     * 
     * @param pos position in the source stream, null for none
     * @param owner owner
     */
    public AsProtocol(StreamPos pos, AsModule owner) {
        OWNER = owner;
        guards = new LinkedList<>();
        actions = new LinkedList<>();
        map = new TreeMap<>();
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Strips module indices from syncing keys.
     * 
     * @param m if not null, checks for name clash with other labels in
     * <code>m</code>, , and if any, returns <code>key</code>
     * @param key a string with possibly an integer value appended
     * @return <code>key</code> with the possible suffix removed
     */
    public static String stripIndex(AsModule m, String key) {
        int pos;
        for(pos = key.length(); pos >= 0; --pos) {
            if(pos == 0) {
                pos = key.length();
                break;
            }
            if(!Character.isDigit(key.charAt(pos - 1)))
                    break;
        }
        String t = key.substring(0, pos);
        if(m != null) {
            for(AsStatement s : m.STATEMENTS) {
                String k = AsLabel.getKey(s.LABEL);
                if(!k.equals(key) && stripIndex(null, k).equals(t))
                    // stripping would cause name clash
                    return key;
            }
        }
        return t;
    }
    /**
     * Appends a set of actions to this protocol.
     * 
     * @param g common guard
     * @param a list of actions
     * @param strip if to strip the index from the action's name
     */
    public void add(AbstractExpression g, Collection<String> a, boolean strip) {
        guards.add(g);
        List<String> stripped = new LinkedList<>();
        for(String l : a) {
            stripped.add(strip ? stripIndex(OWNER, l) : l);
            List<AbstractExpression> e = map.get(l);
            if(e == null) {
                e = new LinkedList<>();
                map.put(l, e);
            }
            e.add(g);
        }
        actions.add(stripped);
    }
    /**
     * Appends another protocol at the end of this one.
     * 
     * @param other the other protocol
     */
    public void add(AsProtocol other) {
        Iterator<AbstractExpression> gI = other.guards.iterator();
        Iterator<List<String>> aI = other.actions.iterator();
        while(gI.hasNext())
            add(gI.next(), aI.next(), false);
    }
    /**
     * If the protocol contains an item "none if Other".
     * 
     * @return if is present an empty action in other case
     */
    public boolean containsOtherNone() {
        Iterator<AbstractExpression> gI = guards.iterator();
        for(List<String> set : actions) {
            AbstractExpression guard = gI.next();
            if(set.size() == 1 && set.get(0).equals(AsLabel.SYNC_KEY_NONE) &&
                    guard instanceof AsOtherExpression)
                return true;
        }
        return false;
    }
}
