/*
 * AsTyped.java
 *
 * Created on Oct 4, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.agent;

/**
 * Some methods for querying the type.
 * 
 * @author Artur Rataj
 */
public interface AsTyped {
    /**
     * Returns if this value is floating.
     * 
     * @return if boolean
     */
    public abstract boolean isFloating();
    /**
     * Returns if this value is boolean.
     * 
     * @return if boolean
     */
    public abstract boolean isBoolean();
    /**
     * Returns an identifier which uniquely specifies the type of the object,
     * a respective subset of types needs to conform to
     * <code>getTypes(boolean, boolean)</code>.
     * 
     * @return type identifier
     */
    public abstract String getTypeId();
    /**
     * Returns an identifier which uniquely specifies the type of a constant
     * whose type is specified by the arguments. Used to implement
     * <code>getTypeId()</code>.
     * 
     * @param isFloating if the constant is of a floating point type
     * @param isBoolean if the constant is of a boolean type
     * @return a string identifier
     */
    public static String getTypeId(boolean isFloating, boolean isBoolean) {
        if(isBoolean)
            return "B";
        else if(isFloating)
            return "R";
        else
            return "Z";
    }
}
